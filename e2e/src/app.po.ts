import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }
  navigateToForm() {
    return browser.get('/form');
  }
  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
