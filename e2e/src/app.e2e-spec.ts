import { AppPage } from './app.po';
import { browser, by, element, ExpectedConditions as EC } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  var firstNumber = element(by.id('first'));
  var secondNumber = element(by.id('second'));
  var goButton = element(by.id('gobutton'));
  
  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });

  it('should fill the form', () => {
    page.navigateToForm();
    firstNumber.sendKeys(1);
    secondNumber.sendKeys(2);
    element(by.buttonText('Go')).click();
    // goButton.click();
    var latestResult = element(by.id('result'));
    browser.wait(EC.presenceOf(latestResult), 10000);
    
    expect(latestResult.getText()).toEqual('3');
  });
});
