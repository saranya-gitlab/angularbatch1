import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { ChartsComponent } from './charts/charts.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NgbAlertComponent } from './ngb-alert/ngb-alert.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { TopnavComponent } from './topnav/topnav.component';
import { GridComponent } from './grid/grid.component';

const routes: Routes = [
  { path: 'form', component: FormComponent},
  { path: 'graph', component: ChartsComponent},
  { path: 'grid', component: GridComponent},
  { path: 'alert', component: NgbAlertComponent},
  { path: 'navbar', component: NavigationbarComponent },
  { path: 'topNav', component:  TopnavComponent  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
