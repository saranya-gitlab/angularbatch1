import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent{

  columnDefs = [
    { field: 'NAME' },
    { field: 'AGE' },
    { field: 'ID'}
];

rowData = [
    { NAME: 'Naveen', AGE: '20', ID: 11 },
    { NAME: 'Raj', AGE: '21', ID: 22 },
    { NAME: 'NaveenRaj', AGE: '22', ID: 33 }
];
}
