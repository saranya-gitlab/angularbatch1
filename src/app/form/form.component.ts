import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

first=0;
second=0;
result=0;

    constructor(private fb: FormBuilder) {}

    ngOnInit() { 
    
    }
    add(){
      this.result = Number(this.first)+Number(this.second);
    }
  
}
