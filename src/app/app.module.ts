import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { AppRoutingModule } from './/app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartsComponent } from './charts/charts.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { SideNavComponent } from './side-nav/side-nav.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbAlertComponent } from './ngb-alert/ngb-alert.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { TopnavComponent } from './topnav/topnav.component';
import { GridComponent } from './grid/grid.component';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    ChartsComponent,
    SideNavComponent,
    NgbAlertComponent,
    NavigationbarComponent,
    TopnavComponent,
    GridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighchartsChartModule,
    NgbModule,
    AgGridModule.withComponents()
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }


