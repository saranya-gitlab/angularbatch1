import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { HttpClient } from '@angular/common/http';
import { interval, Subscription } from 'rxjs';
declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {


  highcharts = Highcharts;
seriesValue =[{
  type: undefined,
  name: 'Year 1800',
  data: [107, 31, 635, 203, 2]
}, {
  type: undefined,
  name: 'Year 1900',
  data: [133, 156, 947, 408, 6]
}, {
  type: undefined,
  name: 'Year 2000',
  data: [814, 841, 3714, 727, 31]
}, {
  type: undefined,
  name: 'Year 2026',
  data: [1216, 1001, 4436, 738, 40]
}];
  chartOptions: Highcharts.Options = {
    title: {
      text: "Infosys stock value"
    },
    xAxis: {
      categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    },
    yAxis: {
      title: {
        text: "Infosys Stock value in dollar"
      }
    },
    series: [{
      data: [12, 8, 43, 35, 20, 90, 100, 110, 90, 80, 70 , 1000],
      type: 'line'
    }]
  }
  barChartOptions: Highcharts.Options = {
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Historic World Population by Region'
    },
    xAxis: {
      categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania']
    },
    yAxis: {
      min: 0,
      title: {
        text: "Infosys Stock value in dollar"
      },
      labels: {
        overflow: 'justify'
      }
    },
    tooltip: {
      valueSuffix: ' millions'
    },
    plotOptions: {
      column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: this.seriesValue
  }
  // public options: any = {
  //   chart: {
  //     type: 'scatter',
  //     height: 700
  //   },
  //   title: {
  //     text: 'Sample Scatter Plot'
  //   },
  //   credits: {
  //     enabled: false
  //   },
  //   tooltip: {
  //     formatter: function() {
  //       return 'x: ' + Highcharts.dateFormat('%e %b %y %H:%M:%S', this.x) +
  //         '  y: ' + this.y.toFixed(2);
  //     }
  //   },
  //   xAxis: {
  //     type: 'datetime',
  //     labels: {
  //       formatter: function() {
  //         return Highcharts.dateFormat('%e %b %y', this.value);
  //       }
  //     }
  //   },
  //   series: [
  //     {
  //       name: 'Normal',
  //       turboThreshold: 500000,
  //       data: []
  //     },
  //     {
  //       name: 'Abnormal',
  //       turboThreshold: 500000,
  //       data: []
  //     }
  //   ]
  // }
  // subscription: Subscription;
  // constructor(private http: HttpClient) { }

  ngOnInit(){}
  //   // Set 10 seconds interval to update data again and again
  //   const source = interval(10000);

  //   // Sample API
  //   const apiLink = 'https://api.myjson.com/bins/13lnf4';

  //   this.subscription = source.subscribe(val => this.getApiResponse(apiLink).then((data:any) => {
  //       const updated_normal_data = [];
  //       const updated_abnormal_data = [];
  //       data.forEach(row => {
  //         const temp_row = [
  //           new Date(row.timestamp).getTime(),
  //           row.value
  //         ];
  //         row.Normal === 1 ? updated_normal_data.push(temp_row) : updated_abnormal_data.push(temp_row);
  //       });
  //       this.options.series[0]['data'] = updated_normal_data;
  //       this.options.series[1]['data'] = updated_abnormal_data;
  //       Highcharts.chart('container', this.options);
  //     },
  //     error => {
  //       console.log('Something went wrong.');
  //     })
  //   );
  // }

  // getApiResponse(url) {
  //   return this.http.get(url, {})
  //     .toPromise().then(res => {
  //       return res;
  //     });
  // }

}
